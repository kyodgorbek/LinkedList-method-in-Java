# LinkedList-method-in-Java

mport java.util.LinkedList;

public class ObjectArrayFromElementsOfLinkedListExample {

 public static void main(String[] args){
  
  //create LinkedList object
    LinkedList lList = new LinkedList();
   
    //add elements to LinkedList
    lList.add("1");
    lList.add("2");
    lList.add("3");
    lList.add("4");
    lList.add("5");
   
 
    /*
     * To create an Object array from elements of Java LinkedList, use
     * Object[] toArray() method.
     *
     * This method returns an array of Objects containing the elements
     * of Java LinkedList in correct order.
     */
   
     Object[] objArray = lList.toArray();
   
     System.out.println("Object array created from Java LinkedList.");
     System.out.println("Object array contains : ");
   
     for(int i=0; i<objArray.length ; i++)
     {
       System.out.println(objArray[i]);
     }
  }
}
